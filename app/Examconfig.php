<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examconfig extends Model
{
    protected $fillable = [
        'outof',
        'contributions',
    ];

}
