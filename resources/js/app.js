/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

Vue.config.warnHandler = function (msg, vm, trace) {
    console.log(msg,trace);
};


import VueRouter from 'vue-router'

// import { routes } from './routes';
const routes = [
    {path: '/', component: require('./components/ExampleComponent.vue').default},
    {path: '/addcourse',name: 'addcourse', component: require('./components/courses/create').default},
    {path: '/printFeeStatement',name: 'printFeeStatement', component: require('./components/reports/feestatement').default},
    {path: '/viewcourse',name: 'viewcourse', component: require('./components/courses/index').default},
    {path: '/addstudent',name: 'addstudent', component: require('./components/student/create').default},
    {path: '/viewstudents',name: 'viewstudents', component: require('./components/student/index').default},
    {path: '/termmanagement',name: 'termmanagement', component: require('./components/terms/create').default},
    {path: '/termregistration',name: 'termregistration', component: require('./components/terms/termregistration').default},
    {path: '/registerstudents/',name: 'registerstudents', component: require('./components/terms/register').default},
    {path: '/showstudents',name: 'showstudents', component: require('./components/student/show').default},
    {path: '/showcourse',name: 'showcourse', component: require('./components/courses/show').default},
];

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.config.silent = true;
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// Mount the vue instance with vue router
const app = new Vue({
    router

}).$mount('#app');
